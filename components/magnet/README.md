## aframe-tocho-magnet-component

[![Version](http://img.shields.io/npm/v/aframe-tocho-magnet-component.svg?style=flat-square)](https://npmjs.org/package/aframe-tocho-magnet-component)
[![License](http://img.shields.io/npm/l/aframe-tocho-magnet-component.svg?style=flat-square)](https://npmjs.org/package/aframe-tocho-magnet-component)

Magnet component to easy positioning

For [A-Frame](https://aframe.io).

### API

| Property | Description | Default Value |
| -------- | ----------- | ------------- |
|          |             |               |

### Installation

#### Browser

Install and use by directly including the [browser files](dist):

```html
<head>
  <title>My A-Frame Scene</title>
  <script src="https://aframe.io/releases/0.8.2/aframe.min.js"></script>
  <script src="https://unpkg.com/aframe-tocho-magnet-component/dist/aframe-tocho-magnet-component.min.js"></script>
</head>

<body>
  <a-scene>
    <a-entity tocho-magnet="foo: bar"></a-entity>
  </a-scene>
</body>
```

#### npm

Install via npm:

```bash
npm install aframe-tocho-magnet-component
```

Then require and use.

```js
require('aframe');
require('aframe-tocho-magnet-component');
```
