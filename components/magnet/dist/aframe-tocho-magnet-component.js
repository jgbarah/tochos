/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

/* global AFRAME */

if (typeof AFRAME === 'undefined') {
  throw new Error('Component attempted to register before AFRAME was available.');
}

/**
 * Tocho Magnet component for A-Frame.
 */
AFRAME.registerComponent('tocho-magnet-preview', {
  schema: {
    objects: {type: 'string', default: ''},
    debug: {type: 'boolean', default: false}
  },

  /**
   * Set if component needs multiple instancing.
   */
  multiple: false,

  /**
   * Called once when component is attached. Generally for initial setup.
   */
  init: function () {
    const el = this.el;
    const data = this.data;
    this.previewEl = null;

    // Add collider
    el.setAttribute('aabb-collider', {'objects': data.objects, 'debug': data.debug});

    // Listener for hitstart: start preview (in place where it would be animated to)
    this.hitstartListener = function (event) {
      console.log('hitstart');
      let collidingEl = el.components['aabb-collider']['closestIntersectedEl'];
      console.log(collidingEl);
      if (this.previewEl) {
        // We had a preview, from other collidingEl. Remove it.
        el.sceneEl.removeChild(this.previewEl);
      };
      this.previewEl = this.previewElement(collidingEl);
    }.bind(this);

    // Listener for hitend: finish preview
    this.hitendListener = function (event) {
      console.log('hitend');
      if (this.previewEl) {
        this.el.sceneEl.removeChild(this.previewEl);
        this.previewEl = null;
      };
    }.bind(this);

    el.addEventListener("hitstart", this.hitstartListener);
    el.addEventListener("hitend", this.hitendListener);
  },

  /**
   * Called when component is attached and when component data changes.
   * Generally modifies the entity based on the data.
   */
  update: function (oldData) { },

  /**
   * Called when a component is removed (e.g., via removeAttribute).
   * Generally undoes all modifications to the entity.
   */
  remove: function () {
    // Remove listeners, collider
    this.el.removeEventListener('animationcomplete', this.animationcompleteListener)
    this.el.removeEventListener("hitstart", this.hitstartListener);
    this.el.removeEventListener("hitend", this.hitendListener);

    this.el.removeAttribute('aabb-collider');    
  },

  /**
   * Called on each scene tick.
   */
  // tick: function (t) { },

  /**
   * Called when entity pauses.
   * Use to stop or remove any dynamic or background behavior such as events.
   */
  pause: function () { },

  /**
   * Called when entity resumes.
   * Use to continue or add any dynamic or background behavior such as events.
   */
  play: function () { },

  /*
   * Find the place (position and rotation) where preview should appear,
   * on top or below bottom of collidingEl
   */
  previewPlace: function (collidingEl) {
    const el = this.el;
    let collidingPos = collidingEl.getAttribute('position');
    let collidingHeight = collidingEl.getAttribute('geometry')['height'];
    let place = {};

    if (this.el.object3D.position.y > collidingPos.y) {
      y = collidingPos.y + collidingHeight;
    } else {
      y = collidingPos.y - collidingHeight;
    }
    place.position = {x: collidingPos.x, y: y, z: collidingPos.z};

    place.rotation = {x: 0, y: 0, z: 0};
    if (el.hasAttribute('rotation')) {
      place.rotation = collidingEl.getAttribute('rotation');
    };
    return place;
  },

  /*
   * Preview element in its place, on top or below bottom of collidingEl
   */
  previewElement: function (collidingEl) {
    const el = this.el;
    
    color = this.el.getAttribute('material')['color'];
    let y = null;
    let rotation = {x: 0, y: 0, z: 0}

    let newEl = document.createElement('a-entity');
    newEl.setAttribute('class', 'magnet-preview');
    newEl.setAttribute('geometry', {'primitive': 'box',
                                    'width': 0.5, 'height': 0.5, 'depth': 0.5});
    newEl.setAttribute('material', {'color': color, 'wireframe': true});
    let place = this.previewPlace(collidingEl);
    newEl.setAttribute('position', place.position);
    newEl.setAttribute('rotation', place.rotation);
    el.sceneEl.appendChild(newEl);
    return newEl;
  }

});

/**
 * Tocho Bounding Box component for A-Frame.
 * 
 * Initializes this.bbox to the bounding box (min, max).
 */
AFRAME.registerComponent('tocho-bounding-box', {
  schema: {
    // Show the bounding box as a wireframe, or
    // solid if color is specified
    show: {type: 'boolean', default: false},
    // Show the bounding box of this color
    color: {type: 'string', default: ''},
    // Show the bounding box with this opacity, if show is true
    opacity: {type: 'number', default: 0.7},
    // Transparency for the bounding box, if show is true
    transparent: {type: 'boolean', default: true},
    // Position for the bounding box, if show is true
    position: {type: 'vec3', default: {x: 0, y: 0, z: 0}},
    // Rotation for the bounding box, if show is true
    rotation: {type: 'vec3', default: {x: 0, y: 0, z: 0}},
    // Show the bounding box as child of this element,
    // if show is true (if empty, attach to this element)
    parent: {type: 'selector', default: ''},
  },

  /**
   * Set if component needs multiple instancing.
   */
  multiple: false,

  /**
   * Called once when component is attached. Generally for initial setup.
   */
  init: function () {
    const me = this;
    const el = this.el;
    const data = this.data;

    el.addEventListener('loaded', function() {
      let object = el.getObject3D('mesh');
      let bbox = new THREE.Box3().setFromObject(object);
      me.bbox =  bbox;
      if (data.show) {
        let material = {};
        let box = document.createElement('a-entity');
        box.setAttribute('geometry',
                         {'primitive': 'box',
                          'width': bbox.max.x - bbox.min.x,
                          'height': bbox.max.y - bbox.min.y,
                          'depth': bbox.max.z - bbox.min.z});
        if (data.color) {
          material.color = data.color;
          material.transparent = data.transparent;
          material.opacity = data.opacity;
          } else {
          material.wireframe = true;
        };
        box.setAttribute('material', material);
        box.setAttribute('position', data.position);
        box.setAttribute('rotation', data.rotation);
        let parent = el;
        if (data.parent) {
          parent = data.parent;
        }
        parent.appendChild(box);
      };
    });
  },

  /**
   * Called when component is attached and when component data changes.
   * Generally modifies the entity based on the data.
   */
  update: function (oldData) { },

  /**
   * Called when a component is removed (e.g., via removeAttribute).
   * Generally undoes all modifications to the entity.
   */
  remove: function () {
  },

  /**
   * Called on each scene tick.
   */
  // tick: function (t) { },

  /**
   * Called when entity pauses.
   * Use to stop or remove any dynamic or background behavior such as events.
   */
  pause: function () { },

  /**
   * Called when entity resumes.
   * Use to continue or add any dynamic or background behavior such as events.
   */
  play: function () { },

});

/***/ })
/******/ ]);