/* global AFRAME */

if (typeof AFRAME === 'undefined') {
  throw new Error('Component attempted to register before AFRAME was available.');
}

/**
 * Tocho Magnet component for A-Frame.
 */
AFRAME.registerComponent('tocho-magnet', {
  schema: {
    targets: {type: 'string', default: ''},
    debug: {type: 'boolean', default: false},
    axis: {type: 'string', default: 'x'},
    side: {type: 'string', default: '+'},
    move: {type: 'boolean', default: false},
  },

  /**
   * Set if component needs multiple instancing.
   */
  multiple: false,

  /**
   * Called once when component is attached. Generally for initial setup.
   */
  init: function () {  },

  update: function (oldData) {
    const el = this.el;
    const data = this.data;

    console.log("Update magnet");

    createBoundingBox = function(evt) {
      if (evt && evt.target != el) {
        // Ignore events in children
        return;
      }
      let mesh = el.getObject3D('mesh');
//      let bbox = new THREE.Box3().setFromObject(mesh);
      mesh.geometry.computeBoundingBox();
      let bbox = mesh.geometry.boundingBox.clone();
      let targetEl = document.querySelector(data.targets);
      let targetMesh = targetEl.getObject3D('mesh');
      targetMesh.geometry.computeBoundingBox();
//      let targetBbox = new THREE.Box3().setFromObject(targetMesh);
      let targetBbox = targetMesh.geometry.boundingBox.clone();
      console.log("Bounding boxes:", bbox, targetBbox, mesh.geometry.boundingBox);
      let position = el.getAttribute('position');
//      let previewPosition = {x: position.x, y: position.y, z: position.z};
      let previewPosition = {x: 0, y: 0, z: 0};
      displacement = (bbox.max[data.axis] - bbox.min[data.axis]) / 2 +
        (targetBbox.max[data.axis] - targetBbox.min[data.axis]) / 2;
      if (data.side == '+') {
        previewPosition[data.axis] = previewPosition[data.axis] + displacement;
      } else {
        previewPosition[data.axis] = previewPosition[data.axis] - displacement;
      }
      console.log("Preview positions:", position, previewPosition);
      if(data.move) {
        targetEl.setAttribute('position', previewPosition);
      } else {
        targetEl.setAttribute('tocho-bounding-box',
        {'position': previewPosition,
         'show': true,
         'parent': el});
      }
    };
    if (el.getObject3D('mesh')) {
      // We already have a mesh, let the fun start
      createBoundingBox();
    } else {
      // We still don't have a mesh, 
      el.addEventListener('object3dset', createBoundingBox);
    };
  },

  /**
   * Called when a component is removed (e.g., via removeAttribute).
   * Generally undoes all modifications to the entity.
   */
  remove: function () {
  },

  /**
   * Called on each scene tick.
   */
  // tick: function (t) { },

  /**
   * Called when entity pauses.
   * Use to stop or remove any dynamic or background behavior such as events.
   */
  pause: function () { },

  /**
   * Called when entity resumes.
   * Use to continue or add any dynamic or background behavior such as events.
   */
  play: function () { },

});

/*
 * Tocho Reparent component
 *
 * Creates a clone element, with the same aspect in the scene
 * (world position, rotation, scale), under the intended parent,
 * and removes this one.
 */
AFRAME.registerComponent('tocho-reparent', {
  schema: {
    parent: {type: 'selector', default: null},
  },
  update: function () {
    const el = this.el;
    const parent = this.data.parent;

    if (el.parentElement == parent) {
      // We're already a child of the intended parent, do nothing
      return;
    };
    // Reparent, once object3D is ready
    reparent = function() {
      // Attach the object3D to the new parent, to get position, rotation, scale
      parent.object3D.attach(el.object3D);
      let position = el.object3D.position;
      let rotation = el.object3D.rotation;
      let scale = el.object3D.scale;

      // Create new element, copy the current one on it
      let newEl = document.createElement(el.tagName);
      if (el.hasAttributes()) {
        let attrs = el.attributes;
        for(var i = attrs.length - 1; i >= 0; i--) {
          let attrName = attrs[i].name;
          let attrVal = el.getAttribute(attrName);
          newEl.setAttribute(attrName, attrVal);
        };
      };
      // Listener for location, rotation,... when the new el is laded
      relocate = function() {
        newEl.object3D.location = location;
        newEl.object3D.rotation = rotation;
        newEl.object3D.scale = scale;
      };
      newEl.addEventListener('loaded', relocate, {'once': true});
      // Attach the new element, and remove this one
      parent.appendChild(newEl);
      el.parentElement.removeChild(el);
    };
    if (el.getObject3D('mesh')) {
      reparent();
    } else {
      el.sceneEl.addEventListener('object3dset', reparent, {'once': true});
    };
  }
});


/**
 * Tocho Bounding Box component for A-Frame.
 * 
 * Initializes this.bbox to the bounding box (min, max).
 * Shows it if "show" is set to true, in color,
 * material, opacity, etc. defined by other properties.
 */
AFRAME.registerComponent('tocho-bounding-box', {
  schema: {
    // Show the bounding box as a wireframe, or
    // solid if color is specified
    show: {type: 'boolean', default: false},
    // Show the bounding box of this color
    color: {type: 'string', default: ''},
    // Show the bounding box with this opacity, if show is true
    opacity: {type: 'number', default: 0.7},
    // Transparency for the bounding box, if show is true
    transparent: {type: 'boolean', default: true},
    // Position for the bounding box, if show is true
    position: {type: 'vec3', default: {x: 0, y: 0, z: 0}},
    // Rotation for the bounding box, if show is true
    rotation: {type: 'vec3', default: {x: 0, y: 0, z: 0}},
    // Show the bounding box as child of this element,
    // if show is true (if empty, attach to this element)
    parent: {type: 'selector', default: ''},
  },

  multiple: false,

  init: function () {
    const me = this;
    const el = this.el;
    const data = this.data;

    let setBoundingBox = function(evt) {
      if (evt && evt.target != el) {
        // Ignore events in children
        return;
      }
      console.log('Init (after object3dset) tocho-building-block');
      let mesh = el.getObject3D('mesh');
//      let bbox = new THREE.Box3().setFromObject(mesh);
      mesh.geometry.computeBoundingBox();
      let bbox = mesh.geometry.boundingBox;
      me.bbox =  bbox;
      if (data.show) {
        let material = {};
        let box = document.createElement('a-entity');
        box.setAttribute('geometry',
                         {'primitive': 'box',
                          'width': bbox.max.x - bbox.min.x,
                          'height': bbox.max.y - bbox.min.y,
                          'depth': bbox.max.z - bbox.min.z});
        console.log('Bbox:', bbox);
        console.log('Dimensions:', bbox.max.x - bbox.min.x, bbox.max.y - bbox.min.y, bbox.max.z - bbox.min.z )
        if (data.color) {
          material.color = data.color;
          material.transparent = data.transparent;
          material.opacity = data.opacity;
          } else {
          material.wireframe = true;
        };
        box.setAttribute('material', material);
        box.setAttribute('position', data.position);
        box.setAttribute('rotation', data.rotation);
        let parent = el;
        if (data.parent) {
          parent = data.parent;
        }
        parent.appendChild(box);
      };
    };
    if (el.getObject3D('mesh')) {
      // We already have a mesh, let the fun start
      setBoundingBox();
    } else {
      // We still don't have a mesh, 
      el.addEventListener('object3dset', setBoundingBox);
    };
  },

});