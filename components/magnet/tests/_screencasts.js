/*
 * Cypress screencasts for bounding-box
 */
// cypress-movie commands
import 'cypress-movie/commands'
//import 'cypress-movie/src/clear-viewport'

describe('Bounding Box component examples (screencasts)', () => {

  beforeEach(() => {
    cy.clearViewport();
//    cy.wait(4000);
  });

  // Tests with 🎥 emoji will be recorded as a gif file
  ['index', 'separate', 'position', 'material', 'parent', 'bounded'].forEach((example) => {
    it(`Video 🎥 (${example})`, () => {
      cy.visit('/examples/bounding-box/' + example + '.html');
      cy.wait(3000);
    });
  });
});
