/*
 * Cypress screenshots for bounding-box
 */

let screenshot = function (dir, files) {
    files.forEach((example) => {
        it(`Screenshot (${example})`, () => {
        cy.visit('/examples/' + dir + '/' + example + '.html');
        cy.wait(3000);
        cy.screenshot(dir + '-' + example);
        });
    });    
};

describe('Bounding box component examples (screenshots)', () => {
    screenshot('bounding-box', ['index', 'separate', 'position', 'material',
                                'parent', 'bounded']);
});

describe('Reparent component examples (screenshots)', () => {
    screenshot('reparent', ['index']);
});
