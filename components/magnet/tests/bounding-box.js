/*
 * Cypress tests for bounding-box component
 */
describe('Bounding Box component', () => {

  beforeEach(() => {
    cy.visit('/tests/index.html');
  });

  it('Creation', () => {
    // Set up scene to test
    cy.get('a-scene').then(scene => {
      let sphere = Cypress.$('<a-entity geometry="primitive: sphere" material="color: red"></a-entity>');
      Cypress.$(scene).append(sphere);
    });
    // Test entities existence
    assert.exists(cy.get('a-entity'));
  });

});

